These designs aren't intended to be anything special. They're simply a few hackish designs that I put together to make my Zebra printers more accessible.

If anything, if you happen to make improvements, let me know. :-)
